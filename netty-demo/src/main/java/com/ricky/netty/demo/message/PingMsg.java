package com.ricky.netty.demo.message;

public class PingMsg extends BaseMsg{

	public PingMsg() {
        super();
        setType(MsgType.PING);
    }
}
